# Development container for simulations
## OS : Ubuntu 22.04 Jammy Jellyfish
## Features added : Desktop-lite, python, git
## Additional Settings : Docker GPU access for hardware acceleration

Access GUI Desktop via tutorial available [here.](https://github.com/devcontainers/features/tree/main/src/desktop-lite)

Additional packages installed. (Check installation-script.sh for detailed view)
1. Mujoco - dm_control
2. Jupyter - ipykernel
